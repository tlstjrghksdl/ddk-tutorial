<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	request.setCharacterEncoding("utf-8");
%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Welcome To Searching</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
	
</script>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
	integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
	integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
	crossorigin="anonymous"></script>
<style>
.ulpadding {
	padding-bottom: 10px;
}
</style>
</head>
<body>
	<header>
		<div class="collapse bg-dark" id="navbarHeader">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-7 py-4">
						<h4 class="text-white" align="left">About</h4>
						<p class="text-muted" align="left">명문대 LolyPolytec Uni는 한국 최고의
							취업률을 자랑하는 학교로서, 교육훈련 현장성 강화를 통해 창의인재를 육성하여 최고의 글로벌 멀티테크니션(Global
							Multi Technician)으로 길러내는 국책기술대학입니다.</p>
					</div>
					<div class="col-sm-4 offset-md-1 py-4">
						<h4 class="text-white" class="ulpadding" align="left">Menu</h4>
						<br>
						<ul class="list-unstyled">
							<li class="ulpadding" align="left"><a href="/DBExample/schoolInform"
								class="text-white">학교정보</a></li>
							<c:choose>
								<c:when test="${member.name eq 'admin'}">
									<li class="ulpadding" align="left"><a
										href="/DBExample/enrolment" class="text-white">과목등록</a></li>
									<li class="ulpadding" align="left"><a
										href="/DBExample/classList" class="text-white">등록된 과목</a></li>
								</c:when>
								<c:otherwise>
									<li class="ulpadding" align="left"><a
										href="/DBExample/enrolment/?grade=${member.grade}"
										class="text-white">수강신청</a></li>
									<li class="ulpadding" align="left"><a
										href="/DBExample/studentInform/?id=${member.id}"
										class="text-white">학생정보</a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${sessionScope.logincheck eq true}">
									<li class="ulpadding" align="left"><a
										href="/DBExample/logout" class="text-white">로그아웃</a></li>
								</c:when>
								<c:otherwise>
									<li class="ulpadding" align="left"><a
										href="/DBExample/login" class="text-white">로그인</a></li>
								</c:otherwise>
							</c:choose>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="navbar navbar-dark bg-dark shadow-sm">
			<div class="container d-flex justify-content-between">
				<a href="/DBExample" class="navbar-brand d-flex align-items-center">
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
						fill="none" stroke="currentColor" stroke-linecap="round"
						stroke-linejoin="round" stroke-width="2" aria-hidden="true"
						class="mr-2" viewBox="0 0 24 24" focusable="false">
						<circle cx="12" cy="13" r="4"></circle></svg> <strong>LolyPolytec
						Uni</strong>
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarHeader" aria-controls="navbarHeader"
					aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>
		</div>
	</header>
	<div style="display: flex;">
		<div style="width: 500px; margin: auto; margin-top: 100px;">
			<c:if test="${result == '회원가입완료'}">
				<div>
					<h1>${result}</h1>
				</div>
				<br>
				<br>
				<h3>아이디(학번)</h3>
				<input type="text" id="inputId" class="form-control"
					value="${student.id}" readonly>
				<br>
				<h3>이름</h3>
				<input type="text" id="inputName" class="form-control"
					value="${student.name}" readonly>
				<br>
				<h3>학년</h3>
				<input type="number" id="inputGrade" class="form-control"
					value="${student.grade}" readonly>
				<br>
				<input type="button" value="확인"
					class="btn btn-lg btn-primary btn-block" id="okay" />
			</c:if>
			<c:if test="${result != '회원가입완료' }">
				<div>
					<h1>${result}</h1>
				</div>
				<input type="button" value="뒤로가기"
					class="btn btn-lg btn-primary btn-block" id="cancel" />
			</c:if>
		</div>
	</div>
	
	<script src="<c:url value="/resources/js/resist.js" />"></script>
</body>
</html>