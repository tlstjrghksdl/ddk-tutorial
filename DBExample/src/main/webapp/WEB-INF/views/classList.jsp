<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width" />
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
	
</script>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
	integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
	integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
	crossorigin="anonymous"></script>

<link href="<c:url value="/resources/style.css" />" rel="stylesheet">
</head>
<body>
	<header>
		<div class="collapse bg-dark" id="navbarHeader">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-7 py-4">
						<h4 class="text-white" align="left">About</h4>
						<p class="text-muted" align="left">명문대 LolyPolytec Uni는 한국 최고의
							취업률을 자랑하는 학교로서, 교육훈련 현장성 강화를 통해 창의인재를 육성하여 최고의 글로벌 멀티테크니션(Global
							Multi Technician)으로 길러내는 국책기술대학입니다.</p>
					</div>
					<div class="col-sm-4 offset-md-1 py-4">
						<h4 class="text-white" class="ulpadding" align="left">Menu</h4><br>
						<ul class="list-unstyled">
							<li class="ulpadding" align="left"><a href="/DBExample/schoolInform" class="text-white">학교정보</a></li>
							<c:choose>
								<c:when test="${member.name eq 'admin'}">
									<li class="ulpadding" align="left">
										<a href="/DBExample/enrolment" class="text-white">과목등록</a>
									</li>
									<li class="ulpadding" align="left">
										<a href="/DBExample/classList" class="text-white">등록된 과목</a>
									</li>
								</c:when>
								<c:otherwise>
									<li class="ulpadding" align="left">
										<a href="/DBExample/enrolment/?grade=${member.grade}" class="text-white">수강신청</a>
									</li>
									<li class="ulpadding" align="left">
										<a href="/DBExample/studentInform/?id=${member.id}" class="text-white">학생정보</a>
									</li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${sessionScope.logincheck eq true}">
									<li class="ulpadding" align="left">
										<a href="/DBExample/logout" class="text-white">로그아웃</a>
									</li>
								</c:when>
								<c:otherwise>
									<li class="ulpadding" align="left">
										<a href="/DBExample/login" class="text-white">로그인</a>
									</li>
								</c:otherwise>
							</c:choose>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="navbar navbar-dark bg-dark shadow-sm">
			<div class="container d-flex justify-content-between">
				<a href="/DBExample" class="navbar-brand d-flex align-items-center"> 
				<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor" stroke-linecap="round"
						stroke-linejoin="round" stroke-width="2" aria-hidden="true" class="mr-2" viewBox="0 0 24 24" focusable="false">
						<circle cx="12" cy="13" r="4"></circle>
				</svg> 
						<strong>LolyPolytec Uni</strong>
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader"
					aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>
		</div>
	</header>
	<div style="margin: 30px;" align="right">${member.name}</div>
	
	<div id="resistOutFrame">
		<div id="resistInFrame">
			<div>
				<h1>등록된 과목</h1>
			</div>
			<table border="1">
				<tr>
					<td id="classFrame" class="tatleColor" align="center">과목</td>
					<td id="classFrame" class="tatleColor" align="center">교수</td>
					<td id="classFrame" class="tatleColor" align="center">학년</td>
					<td id="classFrame" class="tatleColor" align="center">요일</td>
					<td id="classFrame" class="tatleColor" align="center">시작시간</td>
					<td id="classFrame" class="tatleColor" align="center">학점</td>
					<td id="classFrame" class="tatleColor" style="width: 80px;"
						align="center">삭제</td>
				</tr>
				<c:forEach var="classes" items="${classes}">
					<form method="post" action="deleteClass">
					<tr>
						<td id="classFrame" align="center">${classes.classname}</td>
						<td id="classFrame" align="center">${classes.professor}</td>
						<td id="classFrame" align="center">${classes.grade}</td>
						<td id="classFrame" align="center">${classes.day}</td>
						<td id="classFrame" align="center">${classes.time}시</td>
						<td id="classFrame" align="center">${classes.score}</td>
						<td id="classFrame" style="width: 80px;" align="center"><button
								type="submit">삭제</button></td>
					</tr>
					<input type="hidden" name="classname" value="${classes.classname}" />
					<input type="hidden" name="professor" value="${classes.professor}" />
					<input type="hidden" name="grade" value="${classes.grade}" />
					<input type="hidden" name="day" value="${classes.day}" />
					<input type="hidden" name="time" value="${classes.time}" />
					<input type="hidden" name="score" value="${classes.score}" />
					</form>
				</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>