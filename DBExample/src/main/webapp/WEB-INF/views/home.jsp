<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="user-scable= no, width=device-width" />
<title>Home</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
	
</script>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
	integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
	integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
	crossorigin="anonymous"></script>
<style>
.ulpadding {
	padding-bottom: 10px;
}

#resistProgram {
	display: flex;
	justify-content: center;
	height: 100%;
	margin: auto;
}

/* body {
	background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQQAAADCCAMAAACYEEwlAAABd1BMVEX///8HmdjshyD0uiKzwwAAaqrO0N4AltcAldcAaKkAY6cAZqiwwQAAYaYAk9YAVZ3ztgD3+/0AWZ8AXaHrgAAAUJvv9vr0vSLshBTo8PYATJn0uBby8/YAW6Hs9Pn++vX1wDr++OnZ5/FXj7zB1uaWuNTM3ep5pMiPs9Hg56UAcK5muuR+xOjb3edKhbexyd5Bf7MpebGoxdzy9dX42cD1wpb9/vbulTq70uTK1VrtjSD98+jwoSH63qDysnr19+H3y2T87s375tLv8s3vmkRhlcD75rb40nzA4/SY0O06pt3j5OyBqcoAQpVvncPa4ZPP2XPo7bv2yqbysHG7yi7woVv0vInCz0j2yKDQ2m33zpn2yYT1wnDzt1nyrT3vmBf3zl7yriH525X1xEjc5Jnl67EpjMQAg8Uypt330HKbpInLrEMscJlLp84AeMOlml2VlmdzpaG3r1x9jXmCp5eoxLXTq4Sael6Bd3TO5eSp2PBpdIDh3NGG6v5uAAASmElEQVR4nO1di3+aaLrGqmMEDSJqJGk0QUGHpAg2TUwyaS4tzWVce0mazuzsbttku013zuyevZ5zes4ff76Pm4CAICgkk+f3m/k1MSrfw3t53ve7gCARoIURDq/QQhdBKGqmVxMR+mWnYRKchBBCY6ZXExGYMuv0Ei8gnWXHV+8SeuWO00sDDuF+FYaA0OWu00sMxpTpWV5LZKCKfaeXmsvYYJaXMm0QTikAqaQdB0pj+B0yBKItik2n10Te6W1U+i5FBB5rpzGHm0o0OEehgDOOH9l0DKcxBZ3vI71i2+HVQb5C1Gxf6eA9x89sLQsdRxeLI6BZE0La4ZrbeEMQ7V9xEwkdHBdulTVIRRqIIodL7opS397sG0V7C5HRL3cbzt4SQ7B4H6mlBb9vo1wMgcrftqBJ8CgBbl2Y+a5fdko3sUVvkUGosqMq8g8ac0ys8QUnEkhDCC+ct/BbZwiwVuwiUngkUOQtNAQgjklh2bFS8o2+i4KIMehBK7zOQIXkb5VQmgq6v7mVhhAy7lBxeTfBSg0XofvrQBdP44518a8E7fKA6JSjboh1oo0XHbxRQfrFaAM3S3LRstAHZkCRIVYI/kGnMRGLVlEPyn1BjHLejE6jLCJFG5eIAS5WIvx+Kptl3Trbs0FFQCP0yIpQ5OIwa0cVIzQFaZHhY1FZscUQuwb+0Cr3ESLEMi0A2mWn3vq0vxhvRfPFNiDaEaWHLhkfEqJDr9j4lWt2iOZt68Lf4x73uMc97nGPe9xjBJvPHkd9CbPFy9fW3zx+lcrl9md9HUSvF1lJ8TpnIeHZdi6Xym1vev6E+u7Rxpsnl0/Pn14+ebNxtDvZdfQ4vCxGxMLqRcpo+Zuvt1O5VCr3btXb23ePnpw/36qWSlUFpeqDrbeXG76JoAY4x7BoRCu8XuUuDPf80ztgBQAvPb23vvF0Cw7/gRmAiq2nG76ugknjEiVvnIgCj3O5C/2Hby9kCnKpZ17eunEO77sDAA9v6l4vghYWxR6C1LjFaBY8gju/rfxr89OFbASp3IWHmLj7ZGvEAialgU4vt+VgwEQzG/MJhMBX8r9ev1Mo8BQSdy8flFwZUN3i7ZGXi2gXI52K2rxIKSTsb+c0Dl6NfVfdEwUKDT94uIoOSbMRNX0hXsJE8CMIBimVglTu27FvelPySAFE6e14lyD4xUWntfXTx2Nl4N9u6xSkRoSTFUdv7SlQE2TVGimqW+NdotZmolszog4+p3NwMVYs/2AXDsHgt56/BVLpt7/96Xe//8N379+/3xm++MBTYIgKj7XB6xyMSwv181EzqJYenL8xysTVw70PhULy43c7t4GFH3MWDsapxKOdEV1U2rq0G+Lq3kphYSH58b3iERMq6RnAYgjjlfKbB1Z/f3B+5BT36lcLS8nkQhLaQ/Vt2NceGsyGMN4XnpSsFDx1tfPDa8ACpAHkCC+ZMgrsm+0gNY6Dp2YOqqXzcUZePy4kZRp2Hux4ltCzxSuzN4zhoG7JjCVPYnBPZiG58LH0NJRrDhtQLBowpmSqPzeFg+qWxyLxSmOhGssM8czkDGNK5/pbMwdPPYf7L0sqC+dBL3ga2DZ4Q+6du8uafaFa9dErWF3RWJi9KTDtHku59apemiLCmIBg4qD03FfWf6E4RHLp2M+7wgCVLXIoxvGtbpO2XY/ybc6HMzw1+kL10megP1BMIbng723BURmQjV6nxaPFImm3aPO1OT26VwxvTHbgO+MfqqZQOPT7zsBgBaxNIFyb7dh0rMwSIWVtN5uxYbSDkr/WoYyVqPwBoJslB2n7CjVlEcxufZRdg1auPpiAA2RP9YeVKAQT7GLbLhZ8aakeUymXqsGUHCfhADlUSSh47OKHjB5HtkbXrW6mrCTk/uj4EZeGgOAnNRq/L6mS8MLHm2gmtFZTZbBoYwufRizhnVNv9agULB7IuFZMYenK61X3+mL5NyHOw9j6w/6F1RQcQqNRLXvqmdpCTZJLXzz8bYVti2SxKPSnPy/5zErCtv3fGZwhQAl0rJJwMD4ySmKxKHIc1HnTJ2HEIWw145EhHnjoGjvhSiXhevyEhii2WaJLVkBM4Ke9AvyPI6HRVjSeG5whQIdMJSF5PT49yGGcJfn04mKIO7bt8WrEElI2d+loyMHEQRFCI2HFQ46kmn2uiGN8e/o7AbZHSLDLkkNDqAbqiRx7J6Ei4KQw6LnWfmFBnnfMvTKWEBcjprCrR8WA7eIDH5bQZ+hZLdS4UD3AkCVGo8JlNRRnGOoEDzFhhpBba7IDPDaoR0stWd/SDSFYV0hTjEsfvK8AmgFWIQnKVMv+hT4HZ9EKG0NvCDZ3otUOnsTS7LBvMP/Vdw4T0npYrF4G+7Y9rbfkVTbPBjIJmoNuDlOF0Wd3t0IyBD0uFvaCfU7IeGw2/h9zNg6he0NQQ6gXtCpy9q0lN4CkkPtk+FlnweAQw8ZiwDax1mlNLsRrFgpmRtMv9DbLsITQvKH6POC1a1Jp6UOwzwkbz0ZUgdp9zr3TfrGrGULpSbDvWl1IxjIuQkuwVo0aCxo5ens16PzZVUxDAiBhtH/wSQ0LqmTS+uxBSmiITVUuAksI9DnhY99u/vWZLB61Rb6aZg6aG/SwuHQQ7INCx6rtek1FPKr9d63JHLBs0CYdAAl+2qxB0YWFOAWbE4TzAQn2In5TSZVyv1FrLpaCKSVtzsFbCRkaSLg2VIT/a/s+ZlRe1KmYgrYEbyvQxWwu6IYw0/knEnal5H0DLc7/u/ff5ZT2it5aDHQxx1pEmHFuKIoNqUVK3U6z4d8SAPZlb6hrcTFQS0mPip46zSFCEEQumyVxgACHYuokOEqlCs12+wNJkgYtpmc//7+qR0V/s0/BQVRqFE3TLNvrBDm6RYuLb2xfrfUGPIfmUQwCRdG0KHVHeTjQo2I0k7GBseNCQk9K5zEsbUA2i+UxydIg3isMDSFmktkj1BQ5OvdGMVzeRIBOBJbnjZs3Docc+Csgoz6DaghVLFkFI8FwaNZw/1EFWDYNf4tlJd0ptBVbvoUSneZEURD4RkOSIj7uWJXNlh5rj8/rFGBolmu0mG6XWf/lT58hJeA/VFQDUd0QEPwpZqrVGoCA2+DFbDniM+DVAqr63PC7mjSkAM0LDDu0W/r0Zv4zyEqAGmUjz4HBGZK+xSLV7Pb5NMlFfCqXNgVXGnpzZxgM0XR/ZA0FvZbJQB7y8LTLLwYOfKZHuiGiHA/ybvRP16prPRVtBJUWppkByvVtL/DkJpMANOT7m8cGDvx22ns4KTYG/XabYcJbrTIhtMj4k/Ij1dACYhaVnK6ttjaXSHzO5v9s5GDF55RLkxM58CUkSS47P3RpRtCCwh9kh6Y4VDMD0c1R1+cTifk0+rMhKk5SNBCEovmiPDMS4kj1h/dQ59CcFg7yLfcsDm0h8Zf/KEwYEOIG1R92VvYRWlQ5yKbHJS3iF5mFv8ZzwsU3tC7jxw+6HWDi2KUTX37OABISf1q6zXJ5CG1W+rvCf+KqKzTG+ejmQaHwNxAWEvN/hywUoljJGy7UjV87ycI/8jIHYw83OARSeelnmYS/Fe4EB/r8y8fkEmQBG8vBVVK+/XJU+AVycCvrZwueKCx8twBYwNFxZyjuq0q58E9IQubnQryWI0yK+pbCwgIY2V/HHKa5elzQ5hz/rgSFu8GBXkB8hCxcu4me/eMlXR6pQeFfsVqbEwQ/lJTQCMeWPHYY1uaLg+RQIQKdDN1h7nS2VzpN/LukRgXo7Ct7ozXx4d6XhYKBAthGgkJhbj2Cq50Oan/5L5kFTQMXDvYOV5WYv7l6+OLqw1LBwoBOQkwsgaB7QU/s6eZRmYX3+iIDMOaVlevr65UV+O8RAiBT1/MxIYFAqJaQLQc8u4loYGnsv6u6Q+hubzt69aWD/5FjwtdwBjIxaowk9KVyWuoEbM7Qcu38v/BIqY8LTsM2UVBYeYF8hSTMn4QzlknBFstiQ8ChK7DBzmth5CZC/v/eAho8sFBYOoCF8xosoeYibgp1cfgACZzpShwe7JEmAqwe0RaCbDwvjWMBGMGVnDyIhIyIO4RskWUZiSwWMaEfaNcApbQQ4GfUn2xVXVhYKiQPXqj58wTGxcRNxM+ZqnFkeREX2s2gN6MJDSErKsPZ/WHno204BFmy8GFvuBBsHXpD5iziaSRi0GBC2TcihwTDA9Q3floBskDNDEsQ4MeVL3smQV1LBNFK4VEX1icNoCXkjX3V+uHVlwMoEoBU+HBwfPVCk05DnMLcMGlyYLkWG3Vz1QJebrJbnaq+uQqxad8sIM7k9lpmsqE0RRIXW83YzMgCcIAETPA3nK8KB2uTfifd5hYX04Pojqy1gIBTTuP7SWbcyCTMBZFKFNNYXsRj8sjRGuwsov6UhqwWE5mbYN9MUN1GTB4GQi1DEoYTDbWTsTv1KopQCqN6ikl8pGUStOlAYi0zP38zxszXZUOIXCmFCIqEGVKtxokbuX3qnvpOMqEZQlxQgVopr5bE6j3O3Lh4REWJinfJEJQUmVfuqloVgbvsosXXFA7mo24lhIoGUIyoon+/zqskuPjDqfI3k2uEWALWDtgv0P4J9Sa7WcKpxtNdcgag5IFawuQxUQkdTjHhq0pT5k45A4CYTWc/w0Hp3pBwCoynKgd3qNWuoo0CEs4QPeQ5Z4d1laW5uxUQICgum/2cOEGom4QrCfKCrbEZ9Laij6a5xJomgpxC/8mNFg/ulELQAEyBS8x9VZWSPQnEaUaLBzfRr790Bt0fTNhv65Jp3Qrs497JjRY0585iUvTYgWrhZLo44TwAj342kWApC07OMnrMXItvPCC6HN6gKX7CGTma44wkmFQAcbKWGNrJenw5oPkyBwtBftKnC/VM/mCYYqx9HVpBYi4RpJc05W0e/XIRPk6lKS5O/CiNptEflL4ZQZ+s38zNDXPG/FqQcMDguM2JiaGhAx8shLANXAywVPpkzsDCzdraGlANmSEDiUxmLdDsaxNrt4ocMzVr6GI1EBdJsh8kf5/MG0jIQJiiROIs4AS0xNWQpoDz05rCpbFWO10O+PGnRkswIzM3F8wKIJgyTyFEHyentdmniy+6Ls33grWMEwPzN6dhSMSuPN1JC4vTarPXAh9lps4pGd0BGMAciA6nYR2TpnwM0S5G80AwDyBuDCRk1mSsn57QYZcJNFFLR73TxRE1owtMbwEKkRV5Mra1B22Mi27zaxTbbLKTD6MnihHvBXWBKUM6kkAzPJfFsCzXmDgOx2d78ChOPVgC1YK7h7NwszCGSrE16smxPp6EblrbK6fsl4t6O2P4MMkEOxIIPq/tm1f2j2LcnbOFNYcqUkONR9UN5EJDasj7J7GYzKuHBrNWGiVB5QDjJLhylKDa8BABNOKd3mGjcmaSylYSCJmD7PJwAzkLjxCY6EybWcP+JBg71G6MJIzMukuy+XPGDN8Bv1qe/tnTweH9DCbKTIKl0drMy3tGzelAzKajekC0H1SEtNc/pRMmdzC33AkBru3iLCmxhRqXgMYWLM4gHhWuKwkdEvh/3ioLgD9k3ari0GuvydAnaQrjPMlUCwnmeTYJM6xq0tHE3JY/EnwZjUXEEAVExLyVK2YSzLPSNAecYfRcL0CCtizcBpLIcJ6dcYqgSUYqe2xguJHAALu34bKLulhCBcTMXvQygm6DeO65BW8hwbRIgwf1ks3OWRgYHTfUEg008tKC6vA42hdwz9HbQoKxq0IBb8jbNINAisRazldALvOtdidCJogWLjC19rL3STmzTjAt3GpmQfE8mmNozF0nsBKHFfEoW2lEk0C6i2N2fxthVowmydi1T4V9oBhJ1/hfo9kuV440RXTLfpbvm2sHk2Rso3ZmT8H2ynhBSvldPx8u2pyfiT9zFWlSS+CWo6PxFVYTNr82oZJmaCzYTr2g8CfYLHMvhhU5bTsSlL2U49SoVMz6cMnoYZ2AGob1ro07sGl1K6U7iE6s9viMhWUq0tBgk+WxeSw0bKlk71577eu8mYRhZIQ6wVwGsPKJTHba4ZaDNpNgjIxwU73RtbtwXXw6fwvKaL8wTcPByDjMLZ280f+JgXxQHxaTLVyhwjQhCzEMCgSPycfygRBQabZQteP6zfePIL4HePjw4d1ghLCkB6NmlLtrWYwTRU6dc0CFb0bw6PvbT8W6OT1kzpQ7LWOQV8/tVadf8o1RDhQiHkY9jGCwpIdEwji2wfDsWnh0a/uRAwnfPIp6GMFQseZI09i6Ql4+thlD81zLiYFbbwjqaZsaBYl16wA7A14URUFivhk1AzlA3v6IgMDtDHDdorxUKXHWcbzZYLAPTYjzgoMJUDs5hUuVTjvsI+vdVpLhw9mb+/8DS3IxHdkiuvUAAAAASUVORK5CYII=");
	background-attachment: fixed;
} */
</style>
</head>
<body>
	<header>
		<div class="collapse bg-dark" id="navbarHeader">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-md-7 py-4">
						<h4 class="text-white">About</h4>
						<p class="text-muted">명문대 LolyPolytec Uni는 한국 최고의 취업률을 자랑하는
							학교로서, 교육훈련 현장성 강화를 통해 창의인재를 육성하여 최고의 글로벌 멀티테크니션(Global Multi
							Technician)으로 길러내는 국책기술대학입니다.</p>
					</div>
					<div class="col-sm-4 offset-md-1 py-4">
						<h4 class="text-white" class="ulpadding">Menu</h4>
						<br>
						<ul class="list-unstyled">
							<li class="ulpadding"><a href="/DBExample/schoolInform"
								class="text-white">학교정보</a></li>
							<c:choose>
								<c:when test="${member.name eq 'admin'}">
									<li class="ulpadding"><a href="/DBExample/enrolment"
										class="text-white">과목등록</a></li>
									<li class="ulpadding"><a href="/DBExample/classList"
										class="text-white">등록된 과목</a></li>
								</c:when>
								<c:otherwise>
									<li class="ulpadding"><a
										href="/DBExample/enrolment/?grade=${member.grade}"
										class="text-white">수강신청</a></li>
									<li class="ulpadding"><a
										href="/DBExample/studentInform/?id=${member.id}"
										class="text-white">학생정보</a></li>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${sessionScope.logincheck eq true}">
									<li class="ulpadding"><a href="/DBExample/logout"
										class="text-white">로그아웃</a></li>
								</c:when>
								<c:otherwise>
									<li class="ulpadding"><a href="/DBExample/login"
										class="text-white">로그인</a></li>
								</c:otherwise>
							</c:choose>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="navbar navbar-dark bg-dark shadow-sm">
			<div class="container d-flex justify-content-between">
				<a href="/DBExample" class="navbar-brand d-flex align-items-center">
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
						fill="none" stroke="currentColor" stroke-linecap="round"
						stroke-linejoin="round" stroke-width="2" aria-hidden="true"
						class="mr-2" viewBox="0 0 24 24" focusable="false">
						<circle cx="12" cy="13" r="4"></circle>
					</svg> <strong>LolyPolytec Uni</strong>
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarHeader" aria-controls="navbarHeader"
					aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>
		</div>
	</header>
	<div style="margin: 30px;" align="right">${member.name}</div>
	<div style="justify-content: center;">
		<div id="resistProgram" style="display: flex; flex-direction: column; justify-content: center;">
			<div style="display: flex; justify-content: center;"><h1>수강신청 프로그램</h1></div>
			<div style="display: flex; justify-content: center;">
				<img
					src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQQAAADCCAMAAACYEEwlAAABd1BMVEX///8HmdjshyD0uiKzwwAAaqrO0N4AltcAldcAaKkAY6cAZqiwwQAAYaYAk9YAVZ3ztgD3+/0AWZ8AXaHrgAAAUJvv9vr0vSLshBTo8PYATJn0uBby8/YAW6Hs9Pn++vX1wDr++OnZ5/FXj7zB1uaWuNTM3ep5pMiPs9Hg56UAcK5muuR+xOjb3edKhbexyd5Bf7MpebGoxdzy9dX42cD1wpb9/vbulTq70uTK1VrtjSD98+jwoSH63qDysnr19+H3y2T87s375tLv8s3vmkRhlcD75rb40nzA4/SY0O06pt3j5OyBqcoAQpVvncPa4ZPP2XPo7bv2yqbysHG7yi7woVv0vInCz0j2yKDQ2m33zpn2yYT1wnDzt1nyrT3vmBf3zl7yriH525X1xEjc5Jnl67EpjMQAg8Uypt330HKbpInLrEMscJlLp84AeMOlml2VlmdzpaG3r1x9jXmCp5eoxLXTq4Sael6Bd3TO5eSp2PBpdIDh3NGG6v5uAAASmElEQVR4nO1di3+aaLrGqmMEDSJqJGk0QUGHpAg2TUwyaS4tzWVce0mazuzsbttku013zuyevZ5zes4ff76Pm4CAICgkk+f3m/k1MSrfw3t53ve7gCARoIURDq/QQhdBKGqmVxMR+mWnYRKchBBCY6ZXExGYMuv0Ei8gnWXHV+8SeuWO00sDDuF+FYaA0OWu00sMxpTpWV5LZKCKfaeXmsvYYJaXMm0QTikAqaQdB0pj+B0yBKItik2n10Te6W1U+i5FBB5rpzGHm0o0OEehgDOOH9l0DKcxBZ3vI71i2+HVQb5C1Gxf6eA9x89sLQsdRxeLI6BZE0La4ZrbeEMQ7V9xEwkdHBdulTVIRRqIIodL7opS397sG0V7C5HRL3cbzt4SQ7B4H6mlBb9vo1wMgcrftqBJ8CgBbl2Y+a5fdko3sUVvkUGosqMq8g8ac0ys8QUnEkhDCC+ct/BbZwiwVuwiUngkUOQtNAQgjklh2bFS8o2+i4KIMehBK7zOQIXkb5VQmgq6v7mVhhAy7lBxeTfBSg0XofvrQBdP44518a8E7fKA6JSjboh1oo0XHbxRQfrFaAM3S3LRstAHZkCRIVYI/kGnMRGLVlEPyn1BjHLejE6jLCJFG5eIAS5WIvx+Kptl3Trbs0FFQCP0yIpQ5OIwa0cVIzQFaZHhY1FZscUQuwb+0Cr3ESLEMi0A2mWn3vq0vxhvRfPFNiDaEaWHLhkfEqJDr9j4lWt2iOZt68Lf4x73uMc97nGPe9xjBJvPHkd9CbPFy9fW3zx+lcrl9md9HUSvF1lJ8TpnIeHZdi6Xym1vev6E+u7Rxpsnl0/Pn14+ebNxtDvZdfQ4vCxGxMLqRcpo+Zuvt1O5VCr3btXb23ePnpw/36qWSlUFpeqDrbeXG76JoAY4x7BoRCu8XuUuDPf80ztgBQAvPb23vvF0Cw7/gRmAiq2nG76ugknjEiVvnIgCj3O5C/2Hby9kCnKpZ17eunEO77sDAA9v6l4vghYWxR6C1LjFaBY8gju/rfxr89OFbASp3IWHmLj7ZGvEAialgU4vt+VgwEQzG/MJhMBX8r9ev1Mo8BQSdy8flFwZUN3i7ZGXi2gXI52K2rxIKSTsb+c0Dl6NfVfdEwUKDT94uIoOSbMRNX0hXsJE8CMIBimVglTu27FvelPySAFE6e14lyD4xUWntfXTx2Nl4N9u6xSkRoSTFUdv7SlQE2TVGimqW+NdotZmolszog4+p3NwMVYs/2AXDsHgt56/BVLpt7/96Xe//8N379+/3xm++MBTYIgKj7XB6xyMSwv181EzqJYenL8xysTVw70PhULy43c7t4GFH3MWDsapxKOdEV1U2rq0G+Lq3kphYSH58b3iERMq6RnAYgjjlfKbB1Z/f3B+5BT36lcLS8nkQhLaQ/Vt2NceGsyGMN4XnpSsFDx1tfPDa8ACpAHkCC+ZMgrsm+0gNY6Dp2YOqqXzcUZePy4kZRp2Hux4ltCzxSuzN4zhoG7JjCVPYnBPZiG58LH0NJRrDhtQLBowpmSqPzeFg+qWxyLxSmOhGssM8czkDGNK5/pbMwdPPYf7L0sqC+dBL3ga2DZ4Q+6du8uafaFa9dErWF3RWJi9KTDtHku59apemiLCmIBg4qD03FfWf6E4RHLp2M+7wgCVLXIoxvGtbpO2XY/ybc6HMzw1+kL10megP1BMIbng723BURmQjV6nxaPFImm3aPO1OT26VwxvTHbgO+MfqqZQOPT7zsBgBaxNIFyb7dh0rMwSIWVtN5uxYbSDkr/WoYyVqPwBoJslB2n7CjVlEcxufZRdg1auPpiAA2RP9YeVKAQT7GLbLhZ8aakeUymXqsGUHCfhADlUSSh47OKHjB5HtkbXrW6mrCTk/uj4EZeGgOAnNRq/L6mS8MLHm2gmtFZTZbBoYwufRizhnVNv9agULB7IuFZMYenK61X3+mL5NyHOw9j6w/6F1RQcQqNRLXvqmdpCTZJLXzz8bYVti2SxKPSnPy/5zErCtv3fGZwhQAl0rJJwMD4ySmKxKHIc1HnTJ2HEIWw145EhHnjoGjvhSiXhevyEhii2WaJLVkBM4Ke9AvyPI6HRVjSeG5whQIdMJSF5PT49yGGcJfn04mKIO7bt8WrEElI2d+loyMHEQRFCI2HFQ46kmn2uiGN8e/o7AbZHSLDLkkNDqAbqiRx7J6Ei4KQw6LnWfmFBnnfMvTKWEBcjprCrR8WA7eIDH5bQZ+hZLdS4UD3AkCVGo8JlNRRnGOoEDzFhhpBba7IDPDaoR0stWd/SDSFYV0hTjEsfvK8AmgFWIQnKVMv+hT4HZ9EKG0NvCDZ3otUOnsTS7LBvMP/Vdw4T0npYrF4G+7Y9rbfkVTbPBjIJmoNuDlOF0Wd3t0IyBD0uFvaCfU7IeGw2/h9zNg6he0NQQ6gXtCpy9q0lN4CkkPtk+FlnweAQw8ZiwDax1mlNLsRrFgpmRtMv9DbLsITQvKH6POC1a1Jp6UOwzwkbz0ZUgdp9zr3TfrGrGULpSbDvWl1IxjIuQkuwVo0aCxo5ens16PzZVUxDAiBhtH/wSQ0LqmTS+uxBSmiITVUuAksI9DnhY99u/vWZLB61Rb6aZg6aG/SwuHQQ7INCx6rtek1FPKr9d63JHLBs0CYdAAl+2qxB0YWFOAWbE4TzAQn2In5TSZVyv1FrLpaCKSVtzsFbCRkaSLg2VIT/a/s+ZlRe1KmYgrYEbyvQxWwu6IYw0/knEnal5H0DLc7/u/ff5ZT2it5aDHQxx1pEmHFuKIoNqUVK3U6z4d8SAPZlb6hrcTFQS0mPip46zSFCEEQumyVxgACHYuokOEqlCs12+wNJkgYtpmc//7+qR0V/s0/BQVRqFE3TLNvrBDm6RYuLb2xfrfUGPIfmUQwCRdG0KHVHeTjQo2I0k7GBseNCQk9K5zEsbUA2i+UxydIg3isMDSFmktkj1BQ5OvdGMVzeRIBOBJbnjZs3Docc+Csgoz6DaghVLFkFI8FwaNZw/1EFWDYNf4tlJd0ptBVbvoUSneZEURD4RkOSIj7uWJXNlh5rj8/rFGBolmu0mG6XWf/lT58hJeA/VFQDUd0QEPwpZqrVGoCA2+DFbDniM+DVAqr63PC7mjSkAM0LDDu0W/r0Zv4zyEqAGmUjz4HBGZK+xSLV7Pb5NMlFfCqXNgVXGnpzZxgM0XR/ZA0FvZbJQB7y8LTLLwYOfKZHuiGiHA/ybvRP16prPRVtBJUWppkByvVtL/DkJpMANOT7m8cGDvx22ns4KTYG/XabYcJbrTIhtMj4k/Ij1dACYhaVnK6ttjaXSHzO5v9s5GDF55RLkxM58CUkSS47P3RpRtCCwh9kh6Y4VDMD0c1R1+cTifk0+rMhKk5SNBCEovmiPDMS4kj1h/dQ59CcFg7yLfcsDm0h8Zf/KEwYEOIG1R92VvYRWlQ5yKbHJS3iF5mFv8ZzwsU3tC7jxw+6HWDi2KUTX37OABISf1q6zXJ5CG1W+rvCf+KqKzTG+ejmQaHwNxAWEvN/hywUoljJGy7UjV87ycI/8jIHYw83OARSeelnmYS/Fe4EB/r8y8fkEmQBG8vBVVK+/XJU+AVycCvrZwueKCx8twBYwNFxZyjuq0q58E9IQubnQryWI0yK+pbCwgIY2V/HHKa5elzQ5hz/rgSFu8GBXkB8hCxcu4me/eMlXR6pQeFfsVqbEwQ/lJTQCMeWPHYY1uaLg+RQIQKdDN1h7nS2VzpN/LukRgXo7Ct7ozXx4d6XhYKBAthGgkJhbj2Cq50Oan/5L5kFTQMXDvYOV5WYv7l6+OLqw1LBwoBOQkwsgaB7QU/s6eZRmYX3+iIDMOaVlevr65UV+O8RAiBT1/MxIYFAqJaQLQc8u4loYGnsv6u6Q+hubzt69aWD/5FjwtdwBjIxaowk9KVyWuoEbM7Qcu38v/BIqY8LTsM2UVBYeYF8hSTMn4QzlknBFstiQ8ChK7DBzmth5CZC/v/eAho8sFBYOoCF8xosoeYibgp1cfgACZzpShwe7JEmAqwe0RaCbDwvjWMBGMGVnDyIhIyIO4RskWUZiSwWMaEfaNcApbQQ4GfUn2xVXVhYKiQPXqj58wTGxcRNxM+ZqnFkeREX2s2gN6MJDSErKsPZ/WHno204BFmy8GFvuBBsHXpD5iziaSRi0GBC2TcihwTDA9Q3floBskDNDEsQ4MeVL3smQV1LBNFK4VEX1icNoCXkjX3V+uHVlwMoEoBU+HBwfPVCk05DnMLcMGlyYLkWG3Vz1QJebrJbnaq+uQqxad8sIM7k9lpmsqE0RRIXW83YzMgCcIAETPA3nK8KB2uTfifd5hYX04Pojqy1gIBTTuP7SWbcyCTMBZFKFNNYXsRj8sjRGuwsov6UhqwWE5mbYN9MUN1GTB4GQi1DEoYTDbWTsTv1KopQCqN6ikl8pGUStOlAYi0zP38zxszXZUOIXCmFCIqEGVKtxokbuX3qnvpOMqEZQlxQgVopr5bE6j3O3Lh4REWJinfJEJQUmVfuqloVgbvsosXXFA7mo24lhIoGUIyoon+/zqskuPjDqfI3k2uEWALWDtgv0P4J9Sa7WcKpxtNdcgag5IFawuQxUQkdTjHhq0pT5k45A4CYTWc/w0Hp3pBwCoynKgd3qNWuoo0CEs4QPeQ5Z4d1laW5uxUQICgum/2cOEGom4QrCfKCrbEZ9Laij6a5xJomgpxC/8mNFg/ulELQAEyBS8x9VZWSPQnEaUaLBzfRr790Bt0fTNhv65Jp3Qrs497JjRY0585iUvTYgWrhZLo44TwAj342kWApC07OMnrMXItvPCC6HN6gKX7CGTma44wkmFQAcbKWGNrJenw5oPkyBwtBftKnC/VM/mCYYqx9HVpBYi4RpJc05W0e/XIRPk6lKS5O/CiNptEflL4ZQZ+s38zNDXPG/FqQcMDguM2JiaGhAx8shLANXAywVPpkzsDCzdraGlANmSEDiUxmLdDsaxNrt4ocMzVr6GI1EBdJsh8kf5/MG0jIQJiiROIs4AS0xNWQpoDz05rCpbFWO10O+PGnRkswIzM3F8wKIJgyTyFEHyentdmniy+6Ls33grWMEwPzN6dhSMSuPN1JC4vTarPXAh9lps4pGd0BGMAciA6nYR2TpnwM0S5G80AwDyBuDCRk1mSsn57QYZcJNFFLR73TxRE1owtMbwEKkRV5Mra1B22Mi27zaxTbbLKTD6MnihHvBXWBKUM6kkAzPJfFsCzXmDgOx2d78ChOPVgC1YK7h7NwszCGSrE16smxPp6EblrbK6fsl4t6O2P4MMkEOxIIPq/tm1f2j2LcnbOFNYcqUkONR9UN5EJDasj7J7GYzKuHBrNWGiVB5QDjJLhylKDa8BABNOKd3mGjcmaSylYSCJmD7PJwAzkLjxCY6EybWcP+JBg71G6MJIzMukuy+XPGDN8Bv1qe/tnTweH9DCbKTIKl0drMy3tGzelAzKajekC0H1SEtNc/pRMmdzC33AkBru3iLCmxhRqXgMYWLM4gHhWuKwkdEvh/3ioLgD9k3ari0GuvydAnaQrjPMlUCwnmeTYJM6xq0tHE3JY/EnwZjUXEEAVExLyVK2YSzLPSNAecYfRcL0CCtizcBpLIcJ6dcYqgSUYqe2xguJHAALu34bKLulhCBcTMXvQygm6DeO65BW8hwbRIgwf1ks3OWRgYHTfUEg008tKC6vA42hdwz9HbQoKxq0IBb8jbNINAisRazldALvOtdidCJogWLjC19rL3STmzTjAt3GpmQfE8mmNozF0nsBKHFfEoW2lEk0C6i2N2fxthVowmydi1T4V9oBhJ1/hfo9kuV440RXTLfpbvm2sHk2Rso3ZmT8H2ynhBSvldPx8u2pyfiT9zFWlSS+CWo6PxFVYTNr82oZJmaCzYTr2g8CfYLHMvhhU5bTsSlL2U49SoVMz6cMnoYZ2AGob1ro07sGl1K6U7iE6s9viMhWUq0tBgk+WxeSw0bKlk71577eu8mYRhZIQ6wVwGsPKJTHba4ZaDNpNgjIxwU73RtbtwXXw6fwvKaL8wTcPByDjMLZ280f+JgXxQHxaTLVyhwjQhCzEMCgSPycfygRBQabZQteP6zfePIL4HePjw4d1ghLCkB6NmlLtrWYwTRU6dc0CFb0bw6PvbT8W6OT1kzpQ7LWOQV8/tVadf8o1RDhQiHkY9jGCwpIdEwji2wfDsWnh0a/uRAwnfPIp6GMFQseZI09i6Ql4+thlD81zLiYFbbwjqaZsaBYl16wA7A14URUFivhk1AzlA3v6IgMDtDHDdorxUKXHWcbzZYLAPTYjzgoMJUDs5hUuVTjvsI+vdVpLhw9mb+/8DS3IxHdkiuvUAAAAASUVORK5CYII=">
			</div>
		</div>
	</div>
</body>
</html>