$(document).ready(function(){
	$('#cancel').on("click", function() {
		history.back();
	})
	
	$('#okay').on("click", function() {
		location.href = "/DBExample/login";
	})
	
	$('#resist').on("click", function (){
		var passwd = document.getElementById("inputPassword").value;
		if(passwd.length < 4) {
			alert("비밀번호는 4~10자 사이로 설정하세요");
			return false;
		} else if(passwd.length >10) {
			alert("비밀번호는 10자를 초과할 수 없습니다.");
			return false;
		}
		
		var stnName = document.getElementById("inputName").value;

		var blank = /\s+/g;
		var specialCharacters = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
		var number = /[0-9]/;
		if (blank.test(stnName)) {
			alert('이름에 공백을 제거해 주세요');
			return false;
		} else if (specialCharacters
				.test(stnName)) {
			alert('이름에 특수문자를 입력할 수 없습니다.');
			return false;
		} else if (stnName.length == 0) {
			alert('이름을 입력하세요.');
			return false;
		} else if (number.test(stnName)) {
			alert('이름에 숫자를 입력할 수 없습니다.');
			return false;
		}
		
		var grade = document.getElementById("inputGrade").value;
		if(grade.length == 0) {
			alert("학년을 입력하세요");
			return false;
		}
		
		return true;
	})
})