$(document).ready(function() {
	$('#cancel').on("click", function() {
		history.back();
	})

	$('#resist').on("click",function() {
		var stnName = document
				.getElementById("className").value;

		var specialCharacters = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
		var number = /[0-9]/;
		if (specialCharacters
				.test(stnName)) {
			alert('과목명에 특수문자를 입력할 수 없습니다.');
			return false;
		} else if (stnName.length == 0) {
			alert('과목명을 입력하세요.');
			return false;
		} else if (number.test(stnName)) {
			alert('과목명에 숫자를 입력할 수 없습니다.');
			return false;
		} else {
		}

		var professor = document
				.getElementById("professor").value;

		var blank = /\s+/g;
		var specialCharacters = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
		var number = /[0-9]/;
		if (blank.test(professor)) {
			alert('교수님 성함에 공백을 제거해 주세요');
			return false;
		} else if (specialCharacters
				.test(professor)) {
			alert('교수님 성함에 특수문자를 입력할 수 없습니다.');
			return false;
		} else if (professor.length == 0) {
			alert('교수님 성함을 입력하세요.');
			return false;
		} else if (number
				.test(professor)) {
			alert('교수님 성함에 숫자를 입력할 수 없습니다.');
			return false;
		}

		var grade = document
				.getElementById("grade").value;
		if (grade.length == 0) {
			alert("학년을 입력하세요")
			return false;
		}

		var day = document
				.getElementById("day").value;

		var specialCharacters = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
		var number = /[0-9]/;
		if (specialCharacters.test(day)) {
			alert('요일에 콤마(,)외 특수문자를 입력할 수 없습니다.');
			return false;
		} else if (day.length == 0) {
			alert('요일을 입력하세요.');
			return false;
		} else if (number.test(day)) {
			alert('요일에 숫자를 입력할 수 없습니다.');
			return false;
		}

		var time = document
				.getElementById("time").value;
		if (time.length == 0) {
			alert("시간을 입력하세요")
			return false;
		}

		var score = document
				.getElementById("score").value;
		if (score.length == 0) {
			alert("학점을 입력하세요")
			return false;
		}

		return true;
	})
})