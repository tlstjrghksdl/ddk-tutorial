package com.example.dto;

public class ClassVO {
	private String classname;
	private String professor;
	private int grade;
	private String day;
	private int time;
	private int score;

	public String getClassname() {
		return classname;
	}

	public void setClassname(String classname) {
		this.classname = classname;
	}

	public String getProfessor() {
		return professor;
	}

	public void setProfessor(String professor) {
		this.professor = professor;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public ClassVO(String classname, String professor, int grade, String day, int time, int score) {
		super();
		this.classname = classname;
		this.professor = professor;
		this.grade = grade;
		this.day = day;
		this.time = time;
		this.score = score;
	}

	public ClassVO() {
		super();
	}

}
