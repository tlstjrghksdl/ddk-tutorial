package com.example.dto;

public class JoinClassVO {
	private String id;
	private String name;
	private int grade;
	private String classname;
	private String professor;
	private int classgrade;
	private String day;
	private int time;
	private int score;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getClassname() {
		return classname;
	}

	public void setClassname(String classname) {
		this.classname = classname;
	}

	public String getProfessor() {
		return professor;
	}

	public void setProfessor(String professor) {
		this.professor = professor;
	}

	public int getClassgrade() {
		return classgrade;
	}

	public void setClassgrade(int classgrade) {
		this.classgrade = classgrade;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public JoinClassVO(String id, String name, int grade, String classname, String professor, int classgrade, String day,
			int time, int score) {
		super();
		this.id = id;
		this.name = name;
		this.grade = grade;
		this.classname = classname;
		this.professor = professor;
		this.classgrade = classgrade;
		this.day = day;
		this.time = time;
		this.score = score;
	}

	public JoinClassVO() {
		super();
	}

}
