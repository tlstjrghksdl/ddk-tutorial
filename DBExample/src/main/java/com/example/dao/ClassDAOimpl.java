package com.example.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.dto.ClassVO;

@Repository
public class ClassDAOimpl implements ClassDAO {

	@Inject
	private SqlSession sqlSession;
	private static final String Namespace = "com.example.mapper.memberMapper";
	
	@Override
	public int insertClass(ClassVO classVO) throws Exception {
		return sqlSession.insert(Namespace+ ".insertClass", classVO);
	}

	@Override
	public List<ClassVO> selectGrade(String grade) throws Exception {
		return sqlSession.selectList(Namespace+".selectClassGrade", grade);
	}
	

	@Override
	public List<ClassVO> selectAll() throws Exception {
		return sqlSession.selectList(Namespace+".selectAllClass");
	}

	@Override
	public int deleteClass(ClassVO classVO) throws Exception {
		return sqlSession.delete(Namespace + ".deleteClass", classVO);
	}

	@Override
	public int checkClass(ClassVO classVO) throws Exception {
		return sqlSession.selectOne(Namespace + ".checkClass", classVO);
	}

	@Override
	public List<ClassVO> searchClass(String professor) throws Exception {
		return sqlSession.selectList(Namespace + ".searchClass", professor);
	}
	
}
