package com.example.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.dto.JoinClassVO;

@Repository
public class JoinClassDAOimpl implements JoinClassDAO {

	@Inject
	private SqlSession sqlSession;
	private static final String Namespace = "com.example.mapper.memberMapper";

	@Override
	public int insertJoinClass(JoinClassVO joinClassVO) throws Exception {
		return sqlSession.insert(Namespace + ".insertJoinClass", joinClassVO);
	}

	@Override
	public int sumScore(String id) throws Exception {
		if (sqlSession.selectOne(Namespace + ".sumscore", id) == null) {
			return 0;
		} else {
			return sqlSession.selectOne(Namespace + ".sumscore", id);
		}
	}

	@Override
	public List<JoinClassVO> studentInform(String id) throws Exception {
		return sqlSession.selectList(Namespace + ".studentInform", id);
	}

	@Override
	public int deleteJoinClass(JoinClassVO joinClassVO) throws Exception {
		// TODO Auto-generated method stub
		return sqlSession.delete(Namespace + ".deleteJoinClass", joinClassVO);
	}

	@Override
	public int checkJoinClass(JoinClassVO joinClassVO) throws Exception {
		System.out.println();
		return sqlSession.selectOne(Namespace + ".checkJoinClass", joinClassVO);
	}
	
}
