package com.example.dao;

import java.util.List;

import com.example.dto.ClassVO;

public interface ClassDAO {
	public List<ClassVO> selectGrade(String grade) throws Exception;
	public List<ClassVO> selectAll() throws Exception;
	public int insertClass(ClassVO classVO) throws Exception;
	public int deleteClass(ClassVO classVO) throws Exception;
	public int checkClass(ClassVO classVO) throws Exception;	
	public List<ClassVO> searchClass(String professor) throws Exception;
}
