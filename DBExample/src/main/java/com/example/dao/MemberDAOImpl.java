package com.example.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.dto.MemberVO;

@Repository
public class MemberDAOImpl implements MemberDAO {

	@Inject
	private SqlSession sqlSession;
	private static final String Namespace = "com.example.mapper.memberMapper";
	
	@Override
	public List<MemberVO> selectMember() throws Exception {
		return sqlSession.selectList(Namespace+".selectMember");
	}

	@Override
	public int insertMember(MemberVO memberVO) throws Exception {
		return sqlSession.insert(Namespace+".insertMember", memberVO);
	}

	@Override
	public int selectForInsert(int grade) throws Exception {
		return sqlSession.selectOne(Namespace+".selectForInsert", grade);
	}

	@Override
	public MemberVO login(MemberVO memberVO) throws Exception {
		return sqlSession.selectOne(Namespace+".login", memberVO);
	}
	
	
}
