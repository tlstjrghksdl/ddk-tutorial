package com.example.service;

import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Service;
import com.example.dao.MemberDAO;
import com.example.dto.MemberVO;

@Service
public class MemberServiceImpl implements MemberService {

	@Inject
	private MemberDAO dao;

	@Override
	public List<MemberVO> selectMember() throws Exception {

		return dao.selectMember();
	}

	@Override
	public int insertMember(MemberVO memberVO) throws Exception {
		return dao.insertMember(memberVO);
	}

	@Override
	public int selectForInsert(int grade) throws Exception {
		return dao.selectForInsert(grade);
	}

	@Override
	public MemberVO login(MemberVO memberVO) throws Exception {
		return dao.login(memberVO);
	}
	
	
}
