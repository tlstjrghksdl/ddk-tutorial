package com.example.service;

import java.util.List;

import com.example.dto.JoinClassVO;

public interface JoinClassService {
	public int insertJoinClass(JoinClassVO joinClassVO) throws Exception;
	public int sumScore(String id) throws Exception;
	public List<JoinClassVO> studentInform(String id) throws Exception;
	public int deleteJoinClass(JoinClassVO joinClassVO) throws Exception;
	public int checkJoinClass(JoinClassVO joinClassVO) throws Exception;
}
