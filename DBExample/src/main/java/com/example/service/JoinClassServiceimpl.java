package com.example.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.dao.JoinClassDAO;
import com.example.dto.JoinClassVO;

@Service
public class JoinClassServiceimpl implements JoinClassService {

	@Inject
	JoinClassDAO joinDAO;
	
	@Override
	public int insertJoinClass(JoinClassVO joinClassVO) throws Exception {
		return joinDAO.insertJoinClass(joinClassVO);
	}

	@Override
	public int sumScore(String id) throws Exception {
		return joinDAO.sumScore(id);
	}

	@Override
	public List<JoinClassVO> studentInform(String id) throws Exception {
		return joinDAO.studentInform(id);
	}

	@Override
	public int deleteJoinClass(JoinClassVO joinClassVO) throws Exception {
		return joinDAO.deleteJoinClass(joinClassVO);
	}

	@Override
	public int checkJoinClass(JoinClassVO joinClassVO) throws Exception {
		return joinDAO.checkJoinClass(joinClassVO);
	}
	
	
}
