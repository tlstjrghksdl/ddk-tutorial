package com.example.service;

import java.util.List;

import com.example.dto.MemberVO;

public interface MemberService {
	public List<MemberVO> selectMember() throws Exception;
	public int insertMember(MemberVO memberVO) throws Exception;
	public int selectForInsert(int grade) throws Exception;
	public MemberVO login(MemberVO memberVO) throws Exception;
}
