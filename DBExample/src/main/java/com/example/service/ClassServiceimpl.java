package com.example.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.dao.ClassDAO;
import com.example.dto.ClassVO;

@Service
public class ClassServiceimpl implements ClassService {

	@Inject
	private ClassDAO dao;
	
	@Override
	public int insertClass(ClassVO classVO) throws Exception {
		return dao.insertClass(classVO);
	}

	@Override
	public List<ClassVO> selectAll() throws Exception {
		return dao.selectAll();
	}

	
	@Override
	public List<ClassVO> selectGrade(String grade) throws Exception {
		return dao.selectGrade(grade);
	}

	@Override
	public int deleteClass(ClassVO classVO) throws Exception {
		return dao.deleteClass(classVO);
	}

	@Override
	public int checkClass(ClassVO classVO) throws Exception {
		return dao.checkClass(classVO);
	}

	@Override
	public List<ClassVO> searchClass(String professor) throws Exception {
		return dao.searchClass(professor);
	}
	
}
