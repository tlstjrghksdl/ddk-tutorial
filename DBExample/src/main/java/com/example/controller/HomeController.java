package com.example.controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.dto.ClassVO;
import com.example.dto.JoinClassVO;
import com.example.dto.MemberVO;
import com.example.service.ClassService;
import com.example.service.JoinClassService;
import com.example.service.MemberService;

@Controller
public class HomeController {

	@Inject
	private MemberService service;

	@Inject
	private ClassService classService;

	@Inject
	private JoinClassService joinService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(HttpSession session, Model model) throws Exception {
		return "home";
	}

	@RequestMapping(value = "schoolInform", method = RequestMethod.GET)
	public String schoolInform(HttpSession session, Model model) throws Exception {
		return "schoolInform";
	}

	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(HttpSession session, Model model) throws Exception {
		return "login";
	}

	@RequestMapping(value = "login.do", method = RequestMethod.POST)
	public String loginDo(HttpSession session, MemberVO memberVO, Model model) throws Exception {
		MemberVO login = service.login(memberVO);
		if (login != null) {
			session.setAttribute("logincheck", true);
			session.setAttribute("member", login);
			return "redirect:/";
		} else {
			session.setAttribute("logincheck", false);
			model.addAttribute("result", "로그인 실패");
			return "loginAlert";
		}
	}

	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logout(HttpSession session, Model model) throws Exception {
		session.setAttribute("logincheck", false);
		return "login";
	}

	@RequestMapping(value = "resist", method = RequestMethod.GET)
	public String resist(HttpSession session, Model model) throws Exception {
		System.out.println(session);
		return "resist";
	}

	@RequestMapping(value = "resist.do", method = RequestMethod.POST)
	public String resistDo(HttpSession session, Model model, MemberVO memberVO) throws Exception {
		System.out.println(session);
		int chkNum = service.selectForInsert(memberVO.getGrade());
		int studentNum = 0;
		if (chkNum < 100) {
			if (memberVO.getGrade() == 1) {
				studentNum = 200100 + chkNum;
				memberVO.setId(Integer.toString(studentNum));
			} else if (memberVO.getGrade() == 2) {
				studentNum = 200200 + chkNum;
				memberVO.setId(Integer.toString(studentNum));
			} else if (memberVO.getGrade() == 3) {
				studentNum = 200300 + chkNum;
				memberVO.setId(Integer.toString(studentNum));
			} else {
				studentNum = 200400 + chkNum;
				memberVO.setId(Integer.toString(studentNum));
			}
		}

		int result = service.insertMember(memberVO);

		if (result == 1) {
			model.addAttribute("result", "회원가입완료");
			model.addAttribute("student", memberVO);
		} else {
			model.addAttribute("result", "회원가입실패. 다시 시도해주시기 바랍니다.");
		}

		return "resistOkay";
	}

	@RequestMapping(value = "enrolment", method = RequestMethod.GET)
	public String enrolment(@RequestParam(value = "grade", required = false) String grade, HttpSession session,
			Model model) throws Exception {

		if (session.getAttribute("logincheck") == null) {
			model.addAttribute("result", "로그인을 먼저 해주십시오.");
			return "loginAlert";
		} else {

			List<ClassVO> classes = classService.selectGrade(grade);
			model.addAttribute("classes", classes);
			return "enrolment";
		}
	}

	@RequestMapping(value = "resistingClass.do", method = RequestMethod.POST)
	public String resistingClass(HttpSession session, Model model, ClassVO classVO) throws Exception {
		int result = 0;

		int checkClass = classService.checkClass(classVO);
		if (checkClass == 1) {
			model.addAttribute("result", "이미 등록된 과목입니다.");
			return "AddClassResult";
		} else {
			result = classService.insertClass(classVO);
			if (result == 1) {
				model.addAttribute("result", "추가 완료");
				return "AddClassResult";
			} else {
				model.addAttribute("result", "과목 추가 실패");
				return "AddClassResult";
			}
		}
	}

	@RequestMapping(value = "classList", method = RequestMethod.GET)
	public String classList(HttpSession session, Model model) throws Exception {
		List<ClassVO> classes = classService.selectAll();
		model.addAttribute("classes", classes);
		return "classList";
	}

	@RequestMapping(value = "deleteClass", method = RequestMethod.POST)
	public String deleteClass(HttpSession session, Model model, ClassVO classVO) throws Exception {
		int delete = classService.deleteClass(classVO);
		if (delete == 1) {
			model.addAttribute("result", "삭제완료");
		}
		return "deleteAlert";
	}

	@PostMapping(value = "enrolment.do")
	public String enrolmentDo(HttpSession session, Model model, @ModelAttribute ClassVO classVO,
			@ModelAttribute MemberVO memberVO) throws Exception {

		JoinClassVO joinClassVO = new JoinClassVO(memberVO.getId(), memberVO.getName(), memberVO.getGrade(),
				classVO.getClassname(), classVO.getProfessor(), classVO.getGrade(), classVO.getDay(), classVO.getTime(),
				classVO.getScore());
		int result = 0;
		int sumScore = joinService.sumScore(memberVO.getId());
		int checkClass = joinService.checkJoinClass(joinClassVO);
		if (checkClass == 1) {
			model.addAttribute("result", "이미 신청한 과목입니다.");
			return "joinClassAlert";
		} else {
			if (sumScore + classVO.getScore() <= 21) {
				result = joinService.insertJoinClass(joinClassVO);
				System.out.println(result);
				if (result == 1) {
					model.addAttribute("result", "수강신청완료");
					return "joinClassAlert";
				} else {
					model.addAttribute("result", "수강신청실패");
					return "joinClassAlert";
				}
			} else {
				model.addAttribute("result", "신청시 21학점이 넘습니다. 확인 후 다시 신청바랍니다.");
				return "joinClassAlert";
			}
		}
	}

	@RequestMapping(value = "studentInform", method = RequestMethod.GET)
	public String studentInform(@RequestParam(value = "id", required = false) String id, HttpSession session,
			Model model) throws Exception {
		System.out.println(id);
		if (session.getAttribute("logincheck") == null) {
			model.addAttribute("result", "로그인을 먼저 해주십시오.");
			return "loginAlert";
		} else {
			List<JoinClassVO> joinClasses = joinService.studentInform(id);
			System.out.println();
			if (joinClasses.size() != 0) {
				int sumScore = joinService.sumScore(id);
				model.addAttribute("sumScore", sumScore + "학점");
				model.addAttribute("joinClasses", joinClasses);
				return "studentInform";
			} else {
				model.addAttribute("sumScore", "신청한 과목이 없습니다.");
				return "studentInform";
			}
		}
	}

	@RequestMapping(value = "deleteJoinClass", method = RequestMethod.POST)
	public String deleteJoinClass(@RequestParam(value = "id", required = false) String id, HttpSession session,
			Model model, JoinClassVO joinClassVO) throws Exception {
		joinClassVO.setId(id);
		int delete = joinService.deleteJoinClass(joinClassVO);
		if (delete == 1) {
			List<JoinClassVO> joinClasses = joinService.studentInform(id);
			int sumScore = joinService.sumScore(id);
			model.addAttribute("sumScore", sumScore + "학점");
			model.addAttribute("joinClasses", joinClasses);
			return "studentInform";
		} else {
			return "studentInform/?id=" + id;
		}
	}
	
	@RequestMapping(value = "classListForStudent", method = RequestMethod.GET)
	public String classListForStudent(HttpSession session, Model model) throws Exception {
		List<ClassVO> classes = classService.selectAll();
		model.addAttribute("classes", classes);
		return "classListForStudent";
	}
	
	@RequestMapping(value = "searchClass", method = RequestMethod.GET)
	public String searchClass(HttpSession session, Model model, @RequestParam(value = "professor", required = false) String professor) throws Exception {
		List<ClassVO> classes = classService.searchClass(professor);
		model.addAttribute("classes", classes);
		return "searchClass";
	}
}
